# install homebrew and disable analytics
/usr/bin/ruby -e "$curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew analytics off

# make a global gitignore file
git config --global core.excludesfile '~/.gitignore_global'
# Mine looks like this:
.tool-versions/
.projectile
.gitignore
.agignore

# install oh-my-zsh and disable analytics in zshrc
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
export HOMEBREW_NO_ANALYTICS=1

# install asdf
git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.3.0
echo -e '\n. $HOME/.asdf/asdf.sh' >> ~/profiles/asdf.profile
echo -e '\n. $HOME/.asdf/completions/asdf.bash' >> ~/profiles/asdf.profile

# install essential stuff for asdf
brew install coreutils automake autoconf openssl libyaml readline libxslt libtool unixodbc gpg

brew cask install firefox
brew cask install spectacle
# Optional - add the vimFx firefox extension, as well as noscript

# get the projects from github by setting up ssh key then cloning

asdf plugin-add erlang https://github.com/asdf-vm/asdf-erlang.git
asdf plugin-add elixir https://github.com/asdf-vm/asdf-elixir.git
asdf plugin-add nodejs https://github.com/asdf-vm/asdf-nodejs.git
bash ~/.asdf/plugins/nodejs/bin/import-release-team-keyring
asdf plugin-add postgres https://github.com/smashedtoatoms/asdf-postgres.git
asdf install erlang 20.1
asdf global erlang 20.1
asdf install elixir 1.5.2
asdf global elixir 1.5.2
asdf install nodejs 6.11.2
asdf global nodejs 6.11.2
asdf install postgres 9.6.3
asdf global postgres 9.6.3

# start postgres server
pg_ctl start

#Now for the mix and phoenix stuff
mix local.hex
mix archive.install https://github.com/phoenixframework/archives/raw/master/phx_new.ez

# Now for the node and yarn stuff
brew install yarn --without-node
yarn global add exp
yarn global add tern
yarn global add create-react-app

# stackshuttle stuff
brew cask install wkhtmltopdf
# also put the DB_USER exports in your rc or profile

brew tap caskroom/fonts
brew cask install font-source-code-pro

# install expo xde, just follow the site instructions

# get irssi to work with tor
brew install irssi
brew install proxychains-ng
proxychains4 irssi
# default /usr/local/etc/proxychains.conf already specifies socks4 proxy. Can add socks5 line just below JIC.

# installing python
# Basically, brew install pyenv and pyenv-virtualenv
# Install python versions with:
$ pyenv install 3.6.4
# Make virtualenvs with:
$ pyenv virtualenv 3.6.4 tf3
# Make sure your debugger can autocomplete. Add these two lines to ~/.pdbrc
import rlcompleter
pdb.Pdb.complete=rlcompleter.Completer(locals()).complete
# Make sure flake8 is configured the right way:
$ cd ~/.config
$ ln -s ~/.slouchpie_system/configs/flake8 .

    # installing tensorflow
    # see https://github.com/tensorflow/tensorflow/issues/8037
# If you want OpenCL, try this: https://github.com/hughperkins/tf-coriander
$ go get github.com/tensorflow/tensorflow/tensorflow/go
$ cd $GOPATH/src/github.com/tensorflow/tensorflow
$ pyenv virtualenv 3.6.4 tf3
$ pyenv shell tf3
$ ./configure
    # input "-mavx -msse4.1 -msse4.2 -mavx2 -mfma" when prompted for flags during the configuration process
$ bazel build --config=opt //tensorflow/tools/pip_package:build_pip_package
$ bazel-bin/tensorflow/tools/pip_package/build_pip_package /tmp/tensorflow_pkg
$ pyenv virtualenv 3.6.2 tf3
    # The name of the following tmp file will prolly be different for you
$ pip install --ignore-installed --upgrade /tmp/tensorflow_pkg/tensorflow-1.4.0rc0-cp36-cp36m-macosx_10_12_x86_64.whl

make vim look great by
$ ln -s ~/.slouchpie_system/profiles/vimrc ~/.vimrc

### SSH being tidy
ypid has a good comment here:
https://github.com/robbyrussell/oh-my-zsh/issues/5662
for using Include in ssh
The basic idea is to have ur includes in the config file and then his three commented lines.
Then you have dirs in ~/.ssh for each context, e.g. "work"
Each of these dirs has its necessary config in a file called config
You can save unused conf stuff in, e.g. ~/.ssh/work/config.unused

to generate the ssh keys, use this command:
ssh-keygen -t rsa -C "goughjt@tcd.ie" -b 4096 -f github_rsa

### Installing docker on mac
I once upon a time did this with brew and had a load of problems that I cannot recall very well. After much heartbreak and frustration, I learned that most people just install the docker-for-mac app. JFYI.
Also a good thing to do is add
```
"credsStore": "osxkeychain"
```
to your ~/.docker/config.json so that creds go in keychain instead of encrypted in the config file.


### postgres extensions
# timescaledb
https://github.com/timescale/timescaledb
Install from source (make install) and edit postgresql.conf in slouchpie_system to use the lib

### Using R and ESS and REPL
Install ess layer to spacemacs config layers
https://github.com/syl20bnr/spacemacs/tree/master/layers/%2Blang/ess
Now install these globally to R:
devtools
roxygen2
Then use devtools to install colorout:
https://github.com/jalvesaq/colorout
$ devtools::install_github("jalvesaq/colorout")

Symlink the Rprofile to home folder

### sorting out daemons and agents:
There are some things I would rather not have running in the background:
```
cd ~/Library/LaunchAgents
mkdir backups
launchctl unload com.google.keystone.agent.plist
mv com.google.keystone.agent.plist backups/com.google.keystone.agent.plist.bak
```
repeat those last 2 commands for other unwanted agents.
Do similar shit with sudo, relative to the root folder (there will be launchdaemons too).

### getting rid of annoying java shit:
```
sudo rm -rf /Library/Internet\ Plug-Ins/JavaAppletPlugin.plugin
launchctl list | grep java
launchctl remove com.oracle.java.Java-Updater
launchctl remove com.apple.java.InstallOnDemand
```

