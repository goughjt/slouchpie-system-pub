# How slouchpie organises parts of his system

installingStuff.txt is a lot of instructions and tips for installing stuff

###configs

The configs folder contains files, most of which should be symbolically linked into the home directory with a dot before their name, e.g.
```
ln -s ~/.slouchpie_system/configs/zshrc ~/.zshrc
```
Don't forget:
```
flake8 --config=~/./.flake8
```

### profiles

The "profiles" folder is a way for keeping the .bashrc or .zshrc clean (depends on which shell you use). You can see in the zshrc file how it sources these "profiles".

### dmxis preset

The DMXIS presets folder is at:
```
/Library/Application Support/ENTTEC/DMXIS/Presets
```
but sometimes I like to share via dropbox so I do:
```
rm /Library/Application\ Support/ENTTEC/DMXIS/Presets
ln -s  ~/Dropbox/Mainstage/Lights/Presets /Library/Application\ Support/ENTTEC/DMXIS/Presets
```
I also like having my own private presets so I do:
```
rm /Library/Application\ Support/ENTTEC/DMXIS/Presets
ln -s  ~/.slouchpie_system/dmxis_presets /Library/Application\ Support/ENTTEC/DMXIS/Presets
```

