export GOPATH=$HOME/golang
export GOROOT=/usr/local/opt/go/libexec
export GOBIN=$GOPATH/bin
export PATH=$PATH:$GOPATH
export PATH=$PATH:$GOROOT/bin

export TENSORFLOWPATH=$HOME/golang/src/github.com/tensorflow/tensorflow
