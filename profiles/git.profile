gc_commit_message() {
  git add .;
  vim .gc_commit_message;
  git commit -a -F .gc_commit_message;
}
alias gc_commit_message=gc_commit_message;

git_prune_local() {
  git fetch -p && git branch -vv | awk '/: gone]/{print $1}' | xargs git branch -D;
}
alias git_prune_local=git_prune_local;
